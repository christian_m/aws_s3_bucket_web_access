variable "environment" {
  description = "environment where this resource is used"
  type        = string
}

variable "domain_name" {
  description = "domain where the S3 bucket is attached"
  type        = string
}

variable "force_destroy" {
  description = "force destroy of S3 bucket, set to true to prepare a deletion"
  type        = bool
}

variable "index_document_name" {
  description = "filename of the index document"
  type        = string
}

variable "error_document_name" {
  description = "filename of the index document"
  type        = string
}

variable "objects" {
  description = "list of objects to upload to the bucket"
  type        = map(object({
    document_name = string,
    source        = string,
    content_type  = string,
  }))
  default = {}
}