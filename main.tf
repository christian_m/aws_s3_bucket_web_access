module "website_log" {
  source        = "git::git@bitbucket.org:christian_m/aws_s3_bucket_private.git?ref=v1.0"
  bucket_name   = "${var.domain_name}.logs"
  environment   = var.environment
  force_destroy = var.force_destroy
  bucket_acl    = "log-delivery-write"
}

resource "aws_s3_bucket" "website" {
  bucket        = var.domain_name
  acl           = "public-read"
  force_destroy = var.force_destroy
  policy        = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Sid":"PublicReadGetObject",
      "Effect":"Allow",
      "Principal":"*",
      "Action":[
        "s3:GetObject"
      ],
      "Resource":[
        "arn:aws:s3:::${var.domain_name}/*"
      ]
    }
  ]
}
POLICY

  website {
    index_document = var.index_document_name
    error_document = var.error_document_name
  }

  logging {
    target_bucket = module.website_log.bucket_id
    target_prefix = "log/"
  }

  tags = {
    env = var.environment
  }
}

resource "aws_s3_bucket_object" "documents" {
  for_each     = var.objects
  bucket       = aws_s3_bucket.website.id
  key          = each.value.document_name
  source       = each.value.source
  content_type = each.value.content_type

  etag = filemd5(each.value.source)

  tags = {
    env = var.environment
  }
}