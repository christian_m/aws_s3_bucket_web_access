output "bucket_home" {
  value = "http://${var.domain_name}.${aws_s3_bucket.website.website_domain}"
}

output "bucket_domain_name" {
  value = aws_s3_bucket.website.website_domain
}

output "bucket_hosted_zone_id" {
  value = aws_s3_bucket.website.hosted_zone_id
}